[![codecov](https://codecov.io/gh/biji20/message-board-falahsuta/branch/prototype/graph/badge.svg?token=7T71GAVUUP)](https://codecov.io/gh/biji20/message-board-falahsuta)

## Simple Message Board and CLI mailer
Description: Message board where user can send message to each other in each channel they choose.

Project Structure: 
- API-mboard -> serve api for web application
- Web-mboard -> web application for client side
- CLI-mailer -> email send mocking by creating json file and describe receiver, sender, and message body that come from channel that user's subscribe

Long Milestone:
1. Dockerizing apps 
2. Unit and Integration testing
3. CI/CD
4. Documenting app



