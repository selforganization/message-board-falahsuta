const getTimeInfo = require("../date-util.js");

describe("testing for pure date function helpers", () => {
  test("getTimeInfo methods", () => {
    const timeInfoAPI = getTimeInfo(new Date());
    expect(timeInfoAPI).toBeDefined();
    expect(timeInfoAPI).toContain("@");
  });
});
