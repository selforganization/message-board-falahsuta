const DBCursor = require("../connections/mysql");
let db;

const seedDB = async (email, name, channel_name, body) => {
  const user = await db.ExistUser(email);
  const channel = await db.ExistChannels(channel_name);
  if (!user) {
    await db.CreateUser(email, name);
  }

  if (!channel) {
    await db.CreateChannels(channel_name);
  }

  await db.CreateMessage(email, channel_name, body);
};

describe("unit test for db queries used by app", () => {
  beforeAll(() => {
    db = new DBCursor();
  });

  beforeEach(async () => {
    await db.DeleteDataFromTables("message");
    await db.DeleteDataFromTables("user");
    await db.DeleteDataFromTables("channel");
  });

  afterAll((done) => {
    db.con.end();
    done();
  });

  test("DB connections", () => {
    expect(db).toBeInstanceOf(DBCursor);
    expect(db.con).toBeDefined();
  });

  test("ViewChannels method", async () => {
    const channel1 = "channel-1";
    const channel2 = "channel-2";

    // creates 2 channels
    await seedDB("email@t.com", "name", channel1, "body");
    await seedDB("email@t.com", "name", channel2, "body");

    const res = await db.ViewChannels();

    expect(res.length).toEqual(2);
  });

  test("PreViewChannels method", async () => {
    const channel1 = "channel-1";
    const channel2 = "channel-2";

    // creates 2 channels
    await seedDB("email@t.com", "name", channel1, "body");
    await seedDB("email@t.com", "name", channel2, "body");

    // expect the result for 1 channels
    const res = await db.PreViewChannels(channel2);

    expect(res.length).toEqual(1);
  });

  test("IndexChannels method", async () => {
    const channel1 = "channel-1";
    const channel2 = "channel-2";

    // creates 2 channels
    await seedDB("email@t.com", "name", channel1, "body");
    await seedDB("email@t.com", "name", channel2, "body");

    // expect the result for 1 channels
    const res = await db.IndexChannels();

    // has 2 index data
    expect(Object.keys(res).length).toEqual(2);
  });

  test("CreateChannels method", async () => {
    const res = await db.CreateChannels("channel-test");

    // no return value
    expect(res).toBeUndefined();
  });

  test("ExistChannel method", async () => {
    const init = await db.ExistChannels("channel1");

    expect(init).toBeFalsy();

    const channel1 = "channel-1";
    const channel2 = "channel-2";
    // creates 2 channels
    await seedDB("email@t.com", "name", channel1, "body");
    await seedDB("email@t.com", "name", channel2, "body");

    const res = await db.ExistChannels(channel1);

    expect(res).toBeTruthy();
  });

  test("ExistChannel method", async () => {
    const init = await db.ExistChannels("channel1");

    expect(init).toBeFalsy();

    const channel1 = "channel-1";
    const channel2 = "channel-2";
    // creates 2 channels
    await seedDB("email@t.com", "name", channel1, "body");
    await seedDB("email@t.com", "name", channel2, "body");

    const res = await db.ExistChannels(channel1);

    expect(res).toBeTruthy();
  });

  test("CreateUser method", async () => {
    const res = await db.CreateUser("test@test.com", "testname");

    // no return value
    expect(res).toBeUndefined();
  });

  test("ExistUser method", async () => {
    const email = "test@test.com";
    const init = await db.ExistUser(email);

    expect(init).toBeFalsy();

    await seedDB(email, "name", "channel", "body");
    const res = await db.ExistUser(email);

    expect(res).toBeTruthy();
  });

  test("FirstPagePreview method", async () => {
    // creates 2 channels
    await seedDB("email@t.com", "name", "channel-1", "body");
    await seedDB("email@t.com", "name", "channel-2", "body");

    const res = await db.FirstPagePreview();

    // it has two channels in first page view
    expect(res.length).toEqual(2);
  });

  test("DeleteDataFromTables method", async () => {
    // creates 1 channel and delete it afterward
    await db.CreateChannels("channel_test");
    await db.DeleteDataFromTables("channel");

    const res = await db.ViewChannels();

    // no channel appears
    expect(res.length).toEqual(0);
  });

  test("FirstPagePreview method", async () => {
    await seedDB("email@t.com", "name", "channel-1", "body");
    await seedDB("email@t.com", "name", "channel-2", "body");

    const res = await db.FirstPagePreview();
    expect(res.length).toEqual(2);
  });

  test("GetEmailChannel method", async () => {
    await seedDB("email@t.com", "name", "channel-1", "body");
    await seedDB("email@t.com", "name", "channel-2", "body");

    const res = await db.GetEmailChannel("channel-1");
    expect(res[0]).toEqual("email@t.com");
  });

  test("GetEmailBody method", async () => {
    await seedDB("email@t.com", "name", "channel-1", "body");
    await seedDB("email@t.com", "name", "channel-2", "body");

    const res = await db.GetEmailBody();

    // expect to have 2 email to notify
    expect(res.length).toEqual(2);
  });

  test("UpdateEmailSent method", async () => {
    await seedDB("email@t.com", "name", "channel-1", "body");
    await seedDB("email@t.com", "name", "channel-2", "body");

    const res = await db.GetEmailBody();
    const sec = await db.UpdateEmailSent(res[0].message_id);

    // expect no response / not throw error
    expect(sec).toBeUndefined();
  });

  test("DeleteDataFromTables method", async () => {
    await seedDB("email@t.com", "name", "channel-2", "body");

    await db.DeleteDataFromTables("message");
    await db.DeleteDataFromTables("channel");

    const res = await db.ExistChannels("channel-2");

    // that channel have been removed
    expect(res).toBe(false);
  });

  test("seedMessage methods", async () => {
    await seedDB("email@t.com", "name", "channel-1", "body");

    const res = await db.seedMessage(
      "email@t.com",
      "channel-1",
      "anothermessage"
    );
    expect(res.affectedRows).toEqual(1);
  });

  test("seedChannel methods", async () => {
    const res = await db.seedChannels("channel-1");

    // expect no response
    expect(res).toBeUndefined();
  });

  test("seedUser methods", async () => {
    const res = await db.seedUser("email@email.com", "name");

    // expect no response
    expect(res).toBeUndefined();
  });

  test("seedDB methods", async () => {
    const res = await db.seedDB("email@t.com", "name", "channel-1", "body");

    // expect response from another methods example
    const sec = await db.PreViewChannels("channel-1");
    expect(sec[0]["name"]).toEqual("name");
    expect(sec[0]["body"]).toEqual("body");
  });
});
