/**
 *  Return today date
 *  @param {Date} date - input date
 *  @return {string} - return date information in string
 */
function today(date) {
  return (
    (date.getDate() < 10 ? "0" : "") +
    date.getDate() +
    "-" +
    date.toLocaleString("default", { month: "short" }) +
    "-" +
    date.getFullYear()
  );
}

/**
 *  Return current time in details
 *  @param {Date} date - input date
 *  @return {string} - return date information in string
 */
function timeNow(date) {
  return (
    (date.getHours() < 10 ? "0" : "") +
    date.getHours() +
    "-" +
    (date.getMinutes() < 10 ? "0" : "") +
    date.getMinutes() +
    "-" +
    (date.getSeconds() < 10 ? "0" : "") +
    date.getSeconds()
  );
}

/**
 *  Return current time in details to use in timestamp from mailer
 *  @param {Date} date - input date
 *  @return {string} - return date information in string
 */
function getTimeInfo(date) {
  return today(date) + "@" + timeNow(date) + "@" + date.getMilliseconds();
}

module.exports = getTimeInfo;
