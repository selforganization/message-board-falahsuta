var nodeModulesDir = "";
var mysql = require(nodeModulesDir + "mysql");
require(nodeModulesDir + "express-async-errors");
const { v4: uuidv4 } = require(nodeModulesDir + "uuid");

class DBCursor {
  constructor() {
    var mysqlHost = process.env.MYSQL_HOST || "localhost";
    var mysqlPort = process.env.MYSQL_PORT || "3306";
    var mysqlUser = process.env.MYSQL_USER || "root";
    var mysqlPass = process.env.MYSQL_PASS || "password";
    var mysqlDB = process.env.MYSQL_DB || "testdb";

    var connectionOptions = {
      host: mysqlHost,
      port: mysqlPort,
      user: mysqlUser,
      password: mysqlPass,
      database: mysqlDB,
    };

    this.con = mysql.createConnection(connectionOptions);
    this.con.connect(function (err) {
      if (err) throw err;
      console.log("Connected!");
    });
  }

  async ViewChannels() {
    var sql = "select * from channel";
    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }

  async PreViewChannels(channel_name, limit = 0) {
    if (limit === 0) {
      var sql = `select name, body, created_at from message natural join channel natural join user where channel_name="${channel_name}" order by created_at desc`;
    }

    if (limit > 0) {
      var sql = `select name, created_at from message natural join channel natural join user where channel_name="${channel_name}" order by created_at desc LIMIT ${limit}`;
    }

    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }

  async IndexChannels() {
    let index = {};
    const channels = await this.ViewChannels();

    return new Promise((resolve, reject) => {
      channels.map(async (channel, stop) => {
        const limit = 3;
        const messages = await this.PreViewChannels(
          channel.channel_name,
          limit
        );
        index[channel.channel_name] = messages;
        if (stop == channels.length - 1) {
          resolve(index);
        }
      });
    });
  }

  async CreateChannels(name) {
    var sql = `insert into channel (channel_name) values ("${name}")`;
    await this.con.query(sql);
  }

  async ExistChannels(name) {
    var sql = `select * from channel where channel_name="${name}"`;
    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, result) {
        if (err) reject(err);
        if (result.length > 0) resolve(true);
        resolve(false);
      });
    });
  }

  async CreateUser(email, name) {
    var sql = `insert into user (email, name) values ("${email}", "${name}")`;
    await this.con.query(sql);
  }

  async ExistUser(email) {
    var sql = `select * from user where email="${email}"`;
    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, result) {
        if (err) reject(err);
        resolve(result[0]);
      });
    });
  }

  async CreateMessage(email, channel_name, body) {
    var sql = `insert into message(message_id, email, channel_name, body) values ("${uuidv4().slice(
      0,
      6
    )}","${email}", "${channel_name}", "${body}")`;
    await this.con.query(sql);
  }

  async FirstPagePreview() {
    var sql =
      "select channel_name, count(channel_name) as count_msg from message group by channel_name";
    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }

  async DeleteDataFromTables(tables) {
    var sql = `DELETE FROM ${tables}`;
    return new Promise((resolve) => {
      this.con.query(sql, function (err, res) {
        resolve();
      });
    });
  }
}

module.exports = DBCursor;
