const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const cors = require("cors");
const CreatePostRouter = require("./router/create");

// const connect = require("./connection/mysql");
// connect();
// console.log(origin);

// const origin = "http://pw13c.duckdns.org:5901";
// app.use(cors({ origin, optionsSuccessStatus: 200 }));
app.use(cors());

app.use(bodyParser.json());
app.use(CreatePostRouter);

module.exports = app;
