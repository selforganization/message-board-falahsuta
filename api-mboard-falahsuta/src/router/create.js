const express = require("express");
require("express-async-errors");
const router = express.Router();
const DBCursor = require("../../helpers/connections/mysql");

const db = new DBCursor();

router.get("/api/channels", async (req, res) => {
  const channels = await db.FirstPagePreview();

  res.send({ channels });
});

router.get("/api/index", async (req, res) => {
  const channels = await db.IndexChannels();

  res.send({ channels });
});

router.get("/api/channel/:channelname", async (req, res) => {
  const channels = await db.PreViewChannels(req.params.channelname);

  res.send({ channels });
});

router.post("/api/channel/userstatus", async (req, res) => {
  const { email } = req.body;
  const user = await db.ExistUser(email);

  if (!user) {
    return res.send({
      name: "",
    });
  }

  const { name } = user;

  res.send({
    name,
  });
});

router.post("/api/channel/message", async (req, res) => {
  const { email, name, channel_name, body } = req.body;

  const user = await db.ExistUser(email);
  const exist = await db.ExistChannels(channel_name);

  if (!user) {
    await db.CreateUser(email, name);
  }

  if (!exist) {
    await db.CreateChannels(channel_name);
  }

  await db.CreateMessage(email, channel_name, body);
  res.status(201).send({ email, name, channel_name, body });
});

// data seed check only
router.get("/api/test", async (req, res) => {
  const channels = await db.ViewChannels();

  res.send({ channels });
});

// frontend purpose testing only
router.get("/api/delete", async (req, res) => {
  await db.DeleteDataFromTables("message");
  await db.DeleteDataFromTables("user");
  await db.DeleteDataFromTables("channel");

  res.send({ msg: "deleted" });
});

module.exports = router;
