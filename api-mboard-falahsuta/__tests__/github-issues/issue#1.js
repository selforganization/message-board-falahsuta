const request = require("supertest");
const app = require("../../src/app");
const DBCursor = require("../../helpers/connections/mysql");
let db;

describe("github issue #1 : sending emoji breaks the apps because db isnt support some utf version", () => {
  // setup has to conenct to some local mysql database, if not just commenting it out and just bootstrap the db you connect with schema first
  beforeAll(() => {
    db = new DBCursor();
  });

  beforeEach(async () => {
    await db.DeleteDataFromTables("message");
    await db.DeleteDataFromTables("user");
    await db.DeleteDataFromTables("channel");
  });

  afterAll((done) => {
    db.con.end();
    done();
  });

  // issue replicate is when you send message that in the body it contains emoji
  it("sends the error message body that breaks the apps (backend parts)", async () => {
    const data = {
      email: "test@test.com",
      name: "test",
      channel_name: "channeltest1",
      body: "bodytest error 😁",
    };
    const { email, name, channel_name, body } = data;
    try {
      await request(app).post("/api/channel/message").send({
        email,
        name,
        channel_name,
        body,
      });
    } catch (e) {
      expect(e).toMatch("error");
    }
  });
});
