const request = require("supertest");

const customUrl = "http://localhost:4002";

const CreateMessageChannelAndUser = async (email, name, channel_name, body) => {
  await request(customUrl).post("/api/channel/message").send({
    email,
    name,
    channel_name,
    body,
  });
};

describe("api integration testing for apps's functionality", () => {
  beforeAll(async () => {
    // use it with precaution as admin privileges, should've implement admin privileges
    await request(customUrl).get("/api/delete");
  });

  beforeEach(async () => {
    // use it with precaution as admin privileges
    await request(customUrl).get("/api/delete");
  });

  afterAll((done) => {
    done();
  });

  it("creates the message for particular channel and by particular user", async () => {
    const data = {
      email: "test@test.com",
      name: "test",
      channel_name: "channeltest1",
      body: "bodytest",
    };
    const { email, name, channel_name, body } = data;
    await request(customUrl).post("/api/channel/message").send({
      email,
      name,
      channel_name,
      body,
    });

    const response = await request(customUrl)
      .get(`/api/channel/${channel_name}`)
      .send();

    // that one body of message is sent by that particular name
    expect(response.body.channels[0]["name"]).toEqual(name);
    expect(response.body.channels[0]["body"]).toEqual(body);

    await request(customUrl).post("/api/channel/message").send({
      email,
      name,
      channel_name,
      body,
    });

    const SecondMessage = await request(customUrl)
      .get(`/api/channel/${channel_name}`)
      .send();

    // message in that channel is now has two messages
    expect(SecondMessage.body.channels.length).toEqual(2);
  });

  it("creates the channel by posting a message by particular user", async () => {
    const data = {
      email: "test@test.com",
      name: "test",
      channel_name: "channeltest2",
      body: "bodytest",
    };
    const { email, name, channel_name, body } = data;
    await CreateMessageChannelAndUser(email, name, channel_name, body);

    const response = await request(customUrl).get("/api/channels").send();

    // 1 channel creation by posting message
    expect(response.body.channels[0]["channel_name"]).toEqual(channel_name);
    expect(response.body.channels[0]["count_msg"]).toEqual(1);
  });

  it("feeds the data for index page", async () => {
    await CreateMessageChannelAndUser(
      "email@test.com",
      "name",
      "channel_name",
      "body"
    );
    await CreateMessageChannelAndUser(
      "email2@test.com",
      "name2",
      "channel_name2",
      "body2"
    );

    // two index of channels created by user message posting
    const response = await request(customUrl).get("/api/index").send();
    expect(Object.keys(response.body.channels).length).toEqual(2);
  });

  it("sends name from particular email signed in the db", async () => {
    const initResponse = await request(customUrl)
      .post("/api/channel/userstatus")
      .send({
        email: "email@email.com",
      });

    // no corresponding name for that email yet
    expect(initResponse.body.name).toStrictEqual("");

    // signed new user by creating post
    const email = "test@test.com";
    const name = "testhisname";
    await CreateMessageChannelAndUser(email, name, "channel_name", "body");

    const response = await request(customUrl)
      .post("/api/channel/userstatus")
      .send({
        email,
      });

    // there is name for correspond email
    expect(response.body.name).toEqual(name);
  });

  it("is for data seed check", async () => {
    await CreateMessageChannelAndUser(
      "email@email",
      "name",
      "channel_name",
      "body"
    );
    const response = await request(customUrl).get("/api/test").send();

    // seed one data
    expect(response.body.channels.length).toEqual(1);
  });

  it("deletes data for quick front-end testing purpose only, not suits for prod", async () => {
    await CreateMessageChannelAndUser(
      "email@email",
      "name",
      "channel_name",
      "body"
    );
    const response = await request(customUrl).get("/api/delete").send();

    expect(response.body.msg).toEqual("deleted");
  });
});
