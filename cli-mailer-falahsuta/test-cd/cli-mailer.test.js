const saveDir = "./src/email_spool";
const fs = require("fs");

it("writes the files into spool folder", (done) => {
  fs.readdir(saveDir, (err, files) => {
    expect(files.length).toBeGreaterThanOrEqual(0);
    console.log(files);
    done();
  });
});
