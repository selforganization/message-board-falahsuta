function today(date) {
  return (
    (date.getDate() < 10 ? "0" : "") +
    date.getDate() +
    "-" +
    date.toLocaleString("default", { month: "short" }) +
    "-" +
    date.getFullYear()
  );
}

function timeNow(date) {
  return (
    (date.getHours() < 10 ? "0" : "") +
    date.getHours() +
    "-" +
    (date.getMinutes() < 10 ? "0" : "") +
    date.getMinutes() +
    "-" +
    (date.getSeconds() < 10 ? "0" : "") +
    date.getSeconds()
  );
}

function getTimeInfo(date) {
  return today(date) + "@" + timeNow(date) + "@" + date.getMilliseconds();
}

module.exports = getTimeInfo;
