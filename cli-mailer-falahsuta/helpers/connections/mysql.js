var nodeModulesDir = "";
var mysql = require(nodeModulesDir + "mysql");
require(nodeModulesDir + "express-async-errors");
const { v4: uuidv4 } = require(nodeModulesDir + "uuid");

class DBCursor {
  constructor() {
    var mysqlHost = process.env.MYSQL_HOST || "localhost";
    var mysqlPort = process.env.MYSQL_PORT || "3306";
    var mysqlUser = process.env.MYSQL_USER || "root";
    var mysqlPass = process.env.MYSQL_PASS || "password";
    var mysqlDB = process.env.MYSQL_DB || "testdb";

    var connectionOptions = {
      host: mysqlHost,
      port: mysqlPort,
      user: mysqlUser,
      password: mysqlPass,
      database: mysqlDB,
    };

    this.con = mysql.createConnection(connectionOptions);
  }

  async GetEmailChannel(channel_name) {
    var sql = `select email from message where channel_name="${channel_name}" group by email`;
    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, result) {
        if (err) reject(err);
        const results = result.map((el) => el.email);
        resolve(results);
      });
    });
  }

  async GetEmailBody() {
    var sql = `select message_id, email as "from", body, channel_name from message where email_sent is null`;
    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }

  async UpdateEmailSent(message_id) {
    var sql = `UPDATE message SET email_sent=CURRENT_TIMESTAMP where message_id="${message_id}"`;
    await this.con.query(sql);
  }

  async DeleteDataFromTables(tables) {
    var sql = `DELETE FROM ${tables}`;
    return new Promise((resolve) => {
      this.con.query(sql, function (err, res) {
        resolve();
      });
    });
  }

  async seedMessage(email, channel_name, body) {
    var sql = `insert into message(message_id, email, channel_name, body) values ("${uuidv4().slice(
      0,
      6
    )}","${email}", "${channel_name}", "${body}")`;
    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, result) {
        if (err) reject(err);
        resolve(result);
      });
    });
  }

  async seedChannels(name) {
    var sql = `insert into channel (channel_name) values ("${name}")`;
    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, res) {
        resolve();
      });
    });
  }

  async seedUser(email, name) {
    var sql = `insert into user (email, name) values ("${email}", "${name}")`;
    return new Promise((resolve, reject) => {
      this.con.query(sql, function (err, res) {
        resolve();
      });
    });
  }

  async seedDB(email, name, channel_name, body) {
    await this.seedUser(email, name);
    await this.seedChannels(channel_name);
    await this.seedMessage(email, channel_name, body);
  }
}

module.exports = DBCursor;
