const DBCursor = require("../helpers/connections/mysql");
const getTimeInfo = require("../helpers/date-util.js");

class Mailer {
  constructor() {
    this.fs = require("fs");
    this.connect = new DBCursor();
    this.saveLocation = "./src/email_spool/";
  }

  writeSampleData(fileName, objData, cb) {
    let jsonData = JSON.stringify(objData, null, 2);
    this.fs.writeFileSync(this.saveLocation + fileName, jsonData);
    console.log(fileName);
    cb();
  }

  writeEmail() {
    this.connect.GetEmailBody().then((result, err) => {
      result.map(async (el) => {
        const receiver = await this.connect.GetEmailChannel(el.channel_name);
        receiver.map((to) => {
          const nameStamp = getTimeInfo(new Date());
          this.writeSampleData(
            nameStamp + ".json",
            {
              from: el.from,
              to: to,
              body: el.body,
            },
            async () => {
              await this.connect.UpdateEmailSent(el.message_id);
            }
          );
        });
      });
    });
  }
}

module.exports = Mailer;
