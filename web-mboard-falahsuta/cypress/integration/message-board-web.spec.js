import axios from "axios";

describe("the scenario of creating message, channel, and user by submit a form", () => {
  before(async () => {
    // testing purpose
    await axios.get("http://localhost:4002/api/delete");
  });

  it("creates message, channel, user by create post", () => {
    cy.visit("http://localhost:3000");

    cy.contains("Post Message").click();

    // assert
    cy.url().should("include", "/message");
    cy.contains("Post").should("be.visible");
    cy.contains("Next").should("be.disabled");

    cy.get("input[name='email']").click().type("test@test.com");

    // assert
    cy.contains("Next").should("not.be.disabled");

    cy.contains("Next").click();
    cy.get("input[name='name']").click().type("testname");
    cy.get("input[name='channel']").click().type("testchannel");
    cy.get("textarea[name='message']").click().type("message message");
    cy.contains("Next").click();

    // assert
    cy.contains("test@testxx.com").should("be.visible");
    cy.contains("testname").should("be.visible");
    cy.contains("testchannel").should("be.visible");
    // cap by automatic front end
    cy.contains("Message message").should("be.visible");

    cy.contains("Confirm & Continue").click();

    // assert
    cy.url().should("include", "/channel/testchannel");
    cy.contains("testname").should("be.visible");
    cy.contains("testchannel").should("be.visible");
  });

  it("navigates to index page and it loads the message in all channels", () => {
    cy.visit("http://localhost:3000");

    cy.contains("Index").click();
    cy.url().should("include", "/index");
    cy.contains("Index:").should("be.visible");
    cy.contains("testname").should("be.visible");
  });

  it("lists the channel created", () => {
    cy.visit("http://localhost:3000");
    cy.contains("testchannel (1)").should("be.visible");
  });

  it("shows the name of registered user", () => {
    cy.visit("http://localhost:3000");

    cy.contains("Post Message").click();

    // assert
    cy.url().should("include", "/message");

    // assert
    cy.contains("Post").should("be.visible");
    cy.contains("Next").should("be.disabled");

    cy.get("input[name='email']").click().type("test@test.com");
    cy.contains("Next").click();

    // assert
    cy.get("input[name='name']").should("have.value", "testname");
    cy.get("input[name='name']").should("be.disabled");
  });
});
