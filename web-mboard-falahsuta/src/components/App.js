import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Container } from "@material-ui/core";
import IndexView from "./pages/IndexView";
import MainMenu from "./pages/MainMenu";
import ChannelView from "./pages/ChannelView";
import PostMessage from "./pages/PostMessage";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    fontFamily: "Roboto",
    flexWrap: "wrap",
    "& > *": {
      margin: theme.spacing(1),
      borderRadius: "20px",
      width: theme.spacing(59),
      height: theme.spacing(55),
      // boxShadow: "0px 8px 45px 0 rgba(31, 31, 31, 0.15)",
    },
  },
}));

const App = () => {
  const classes = useStyles();

  const spacing = (num) => {
    return <div style={{ marginTop: "3px", width: "30px", height: num }}></div>;
  };

  return (
    <>
      {spacing(25)}
      <Container>
        <Router>
          <Switch>
            <div style={{ fontFamily: "Roboto" }}>
              <Route exact path="/" component={MainMenu} />
              <Route exact path="/channel/:channel" component={ChannelView} />
              <Route exact path="/index" component={IndexView} />
              <Route exact path="/message" component={PostMessage} />
            </div>
          </Switch>
        </Router>
      </Container>
    </>
  );
};

export default React.memo(App);
