import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: 50,
    cursor: "pointer",
  },
  details: {
    marginBottom: "300px",
  },
}));

export default function MediaControlCard(props) {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Card className={classes.root} elevation={1}>
      <div className={classes.details}>
        <CardContent>
          <Typography variant="subtitle2" color="textSecondary">
            {`${props.name} (${props.count})`}
          </Typography>
        </CardContent>
      </div>
    </Card>
  );
}
