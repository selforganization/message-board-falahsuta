import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: 120,
    userSelect: "none",
  },
  details: {
    marginBottom: "300px",
  },
}));

export default function MediaControlCard(props) {
  const classes = useStyles();
  const theme = useTheme();

  const getHourAgo = (date) => {
    return Math.round(Math.abs(new Date() - new Date(date)) / 36e5);
  };

  return (
    <Card className={classes.root} elevation={1}>
      <div className={classes.details}>
        <CardContent>
          <Typography variant="subtitle1">{props.chname}</Typography>
          {props.data.map((el) => {
            const ago = getHourAgo(new Date(el.created_at));

            return (
              <Typography
                key={el.created_at}
                variant="subtitle2"
                color="textSecondary"
              >
                {`${el.name} - ${
                  ago <= 1 ? "less than an hour ago" : `${ago} hours ago`
                }`}
              </Typography>
            );
          })}
        </CardContent>
      </div>
    </Card>
  );
}
