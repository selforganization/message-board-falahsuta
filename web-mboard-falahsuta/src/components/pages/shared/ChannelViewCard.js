import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    height: 70,
    cursor: "pointer",
  },
  details: {
    marginBottom: "300px",
  },
}));

export default function MediaControlCard(props) {
  const classes = useStyles();
  const theme = useTheme();

  const getHourAgo = (date) => {
    return Math.round(Math.abs(new Date() - new Date(date)) / 36e5);
  };

  const ago = getHourAgo(new Date(props.created));

  return (
    <Card className={classes.root} elevation={1}>
      <div className={classes.details}>
        <CardContent>
          <Typography variant="subtitle2" color="textSecondary">
            {`${props.name} - ${
              ago <= 1 ? "less than an hour ago" : `${ago} hours ago`
            }`}
          </Typography>
          <Typography variant="subtitle2" color="textSecondary">
            {props.body}
          </Typography>
        </CardContent>
      </div>
    </Card>
  );
}
