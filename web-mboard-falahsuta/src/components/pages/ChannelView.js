import React, { useEffect, useState } from "react";
import axios from "axios";
import { Grid, Button, Breadcrumbs, Link, Typography } from "@material-ui/core";
import ChannelViewCard from "./shared/ChannelViewCard";
import { useHistory } from "react-router-dom";

const MainMenu = (props) => {
  const [channels, setChannels] = useState([]);
  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      const response = await axios.get(
        `http://localhost:4002/api/channel/${props.match.params.channel}`
      );
      setChannels(response.data.channels);
    }
    fetchData();
  }, []);

  return (
    <>
      <Grid container direction="column" justify="center" alignItems="center">
        <Grid>
          <Breadcrumbs separator="›" aria-label="breadcrumb">
            <Link
              color="inherit"
              style={{ cursor: "pointer" }}
              onClick={() => history.push("/")}
            >
              Home
            </Link>

            <Typography color="textPrimary">Channel</Typography>
          </Breadcrumbs>
        </Grid>
        <Grid>
          <h4
            style={{
              color: "rgb(130,130,130,1)",
              fontWeight: 400,
              marginRight: "320px",
              userSelect: "none",
            }}
          >
            {props.match.params.channel}
          </h4>
          {channels.map((el) => {
            return (
              <div style={{ marginBottom: "7px" }}>
                <ChannelViewCard
                  name={el.name}
                  body={el.body}
                  created={el.created_at}
                  key={el.created_at}
                />
              </div>
            );
          })}
        </Grid>
      </Grid>
    </>
  );
};

export default React.memo(MainMenu);
