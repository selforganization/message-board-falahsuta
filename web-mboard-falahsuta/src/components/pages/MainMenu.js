import React, { useEffect, useState } from "react";
import axios from "axios";
import { Grid, Button } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import ChannelCard from "./shared/ChannelCard";

const MainMenu = () => {
  const history = useHistory();
  const [channels, setChannels] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const response = await axios.get("http://localhost:4002/api/channels");
      // const other = await axios.get("http://localhost:3001/api/test");
      setChannels(response.data.channels);
      // console.log(other.data.channels);
    }

    fetchData();
  }, []);

  return (
    <>
      <Grid
        container
        direction="column"
        justify="flex-start"
        alignItems="center"
      >
        <Grid>
          <Button
            style={{ color: "rgb(130,130,130,0.9)", marginBottom: "5px" }}
            onClick={() => history.push("/index")}
          >
            Index
          </Button>
        </Grid>
        <Grid>
          <Button
            onClick={() => history.push("/message")}
            style={{ color: "rgb(130,130,130,0.9)", marginBottom: "20px" }}
          >
            Post Message
          </Button>
        </Grid>
        <Grid>
          <h4
            style={{
              color: "rgb(130,130,130,1)",
              fontWeight: 400,
              marginRight: "320px",
              userSelect: "none",
            }}
          >
            Channels:
          </h4>
          {channels.map((el) => {
            return (
              <div
                onClick={() => history.push(`/channel/${el.channel_name}`)}
                style={{ marginBottom: "7px" }}
              >
                <ChannelCard name={el.channel_name} count={el.count_msg} />
              </div>
            );
          })}
        </Grid>
      </Grid>
    </>
  );
};

export default React.memo(MainMenu);
