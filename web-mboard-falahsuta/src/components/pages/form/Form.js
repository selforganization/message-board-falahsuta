import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import CssBaseline from "@material-ui/core/CssBaseline";
import Paper from "@material-ui/core/Paper";
import StepForm from "./StepForm";

const styles = (theme) => ({
  layout: {
    width: "auto",
    // marginLeft: theme.spacing(2),
    // marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 550,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      // marginTop: theme.spacing(8),
      // marginBottom: theme.spacing(8),
      padding: theme.spacing(3),
    },
  },
});

const App = ({ classes, closeAll }) => {
  return (
    <div className="App">
      <CssBaseline />
      <main className={classes.layout}>
        <Paper className={classes.paper} elevation={0}>
          <StepForm closeAll={closeAll} />
        </Paper>
      </main>
    </div>
  );
};

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(App);
