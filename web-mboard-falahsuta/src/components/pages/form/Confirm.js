import React, { Fragment } from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import axios from "axios";
import { useHistory } from "react-router-dom";

// Destructure props
const Confirm = ({
  handleNext,
  prename,
  handleBack,
  values: { email, name, channel, message },
}) => {
  const history = useHistory();
  const cap = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  const handleSend = async () => {
    const packet = {
      email,
      channel_name: channel,
      body: message,
      name: prename ? prename : name,
    };
    await axios.post("http://localhost:4002/api/channel/message", packet);
    history.push(`/channel/${channel}`);
  };

  return (
    <Fragment>
      <List disablePadding>
        <ListItem>
          <ListItemText primary="Email" secondary={email} />
        </ListItem>

        <Divider />

        <ListItem>
          <ListItemText primary="Name" secondary={prename ? prename : name} />
        </ListItem>

        <Divider />

        <ListItem>
          <ListItemText primary="Channel" secondary={channel} />
        </ListItem>

        <Divider />

        <ListItem>
          <ListItemText primary="Message" secondary={cap(message)} />
        </ListItem>

        <Divider />
      </List>

      <div
        style={{ display: "flex", marginTop: 50, justifyContent: "flex-end" }}
      >
        <Button variant="contained" color="default" onClick={handleBack}>
          Back
        </Button>
        <Button
          style={{ marginLeft: 20 }}
          variant="contained"
          color="primary"
          onClick={() => {
            handleSend();
            // handleNext();
          }}
        >
          Confirm & Continue
        </Button>
      </div>
    </Fragment>
  );
};

export default Confirm;
