import React, { useState, Fragment } from "react";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import FirstStep from "./FirstStep";
import SecondStep from "./SecondStep";
import Confirm from "./Confirm";
import Success from "./Success";
import channelValidateFunc from "./channel-validate";

const emailRegex = RegExp(/^[^@]+@[^@]+\.[^@]+$/);
const labels = ["User", "Content & Channel", "Confirmation"];

const StepForm = (props) => {
  const [steps, setSteps] = useState(0);

  // predefined name of users;
  const [userName, setUserName] = useState("");

  const [fields, setFields] = useState({
    name: "",
    email: "",
    message: "",
    channel: "",
  });

  // Copy fields as they all have the same name
  const [filedError, setFieldError] = useState({
    ...fields,
  });

  const [isError, setIsError] = useState(false);

  // Proceed to next/previous step
  const handleNext = () => setSteps(steps + 1);
  const handleBack = () => setSteps(steps - 1);

  const handleName = (name) => {
    setUserName(name);
  };

  // Handle fields change
  const handleChange = (input) => ({ target: { value } }) => {
    // Set values to the fields
    setFields({
      ...fields,
      [input]: value,
    });

    // Handle errors
    const formErrors = { ...filedError };
    const messageValidate = value.length <= 32 && value.length >= 8;
    const nameValidate = value.length >= 3;
    const channelValidate = channelValidateFunc(value);

    switch (input) {
      case "email":
        formErrors.email = emailRegex.test(value) ? "" : "Not a valid email";
        break;
      case "message":
        formErrors.message = messageValidate
          ? ""
          : "Message can only contain 32 characters long and minimum 8 characters";
        break;
      case "channel":
        formErrors.channel = channelValidate ? "" : "lowercase, angka, -, _";
        break;
      case "name":
        formErrors.name = nameValidate ? "" : "Required minimum 3 characters";
        break;
      default:
        break;
    }

    // set error hook
    Object.values(formErrors).forEach((error) =>
      error.length > 0 ? setIsError(true) : setIsError(false)
    );

    // set errors hook
    setFieldError({
      ...formErrors,
    });
  };

  const handleSteps = (step) => {
    switch (step) {
      case 0:
        return (
          <FirstStep
            handleNext={handleNext}
            handleChange={handleChange}
            values={fields}
            isError={isError}
            filedError={filedError}
          />
        );
      case 1:
        return (
          <SecondStep
            handleNext={handleNext}
            handleBack={handleBack}
            handleChange={handleChange}
            values={fields}
            isError={isError}
            filedError={filedError}
            handleName={handleName}
          />
        );
      case 2:
        return (
          <Confirm
            handleNext={handleNext}
            handleBack={handleBack}
            values={fields}
            prename={userName}
          />
        );
      default:
        break;
    }
  };

  // Handle components
  return (
    <div>
      {steps === labels.length ? (
        <Success closeAll={props.closeAll} />
      ) : (
        <Fragment>
          <Stepper
            activeStep={steps}
            style={{ paddingTop: 30, paddingBottom: 50 }}
            alternativeLabel
          >
            {labels.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          {handleSteps(steps)}
        </Fragment>
      )}
    </div>
  );
};

export default StepForm;
