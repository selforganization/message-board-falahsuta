// Fungsi validasi nama channel

function checkLower(a) {
  for (let i = 0; i < a.length; i++) {
    if (a[i] === a[i].toUpperCase()) {
      return false;
    }
  }
  return true;
}

function afterNum(a) {
  let i = 0;
  let idx = 0;
  while (i < a.length) {
    if (!isNaN(a[i])) {
      idx = i;
    }
    i += 1;
  }

  while (idx < a.length - 1 && !isNaN(a[idx])) {
    idx += 1;
  }
  return idx - 1;
}

function isAlphaOrParen(str) {
  return /^[a-zA-Z()]+$/.test(str);
}

function newSeperate(a) {
  let answer = a.replace(/[^a-z]/gi, "");
  return answer;
}

function hasNumber(myString) {
  return /\d/.test(myString);
}

function validate(a) {
  if (!checkLower(newSeperate(a))) {
    return false;
  }
  if (!isNaN(a[afterNum(a) + 1])) {
    return true;
  }
  if (hasNumber(a)) {
    if (
      isAlphaOrParen(a[afterNum(a) + 1]) ||
      !(a[afterNum(a) + 1].includes("-") || a[afterNum(a) + 1].includes("_"))
    ) {
      return false;
    }
  }

  return true;
}

export default validate;
