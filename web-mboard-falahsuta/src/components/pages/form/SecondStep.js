import React, { Fragment, useEffect, useState } from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";
import channelValidate from "./channel-validate";

// Destructure props
const SecondStep = ({
  handleNext,
  handleBack,
  handleName,
  handleChange,
  values: { message, email, name, channel },
  filedError,
  isError,
}) => {
  // Check if all values are not empty
  const [username, setUserName] = useState(null);

  useEffect(() => {
    async function fetchName() {
      const response = await axios.post(
        "http://localhost:4002/api/channel/userstatus",
        { email }
      );
      setUserName(response.data.name);
      if (response.data.name != "") {
        handleName(response.data.name);
      }
    }
    fetchName();
  });

  const isEmpty =
    username && username.length > 0
      ? message.length >= 8 &&
        message.length <= 32 &&
        channel.length >= 3 &&
        channelValidate(channel)
      : message.length >= 8 &&
        message.length <= 32 &&
        channel.length >= 3 &&
        name.length >= 3 &&
        channelValidate(channel);
  return (
    <Fragment>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <TextField
            fullWidth
            label="Email"
            name="email"
            placeholder="email"
            defaultValue={email}
            onChange={handleChange("email")}
            margin="normal"
            error={filedError.email !== ""}
            helperText={filedError.email !== "" ? `${filedError.email}` : ""}
            disabled
            required
          />
        </Grid>
        {(username === "" || (username && username.length > 0)) && (
          <>
            <Grid item xs={12} sm={12}>
              <TextField
                fullWidth
                label="Name"
                name="name"
                placeholder="name"
                defaultValue={username}
                onChange={handleChange("name")}
                margin="normal"
                error={filedError.name !== ""}
                helperText={filedError.name !== "" ? `${filedError.name}` : ""}
                required
                disabled={username.length > 0 ? true : false}
              />
            </Grid>
          </>
        )}
        <Grid item xs={12} sm={12}>
          <TextField
            fullWidth
            label="Channel"
            name="channel"
            placeholder="channel"
            defaultValue={channel}
            onChange={handleChange("channel")}
            margin="normal"
            error={filedError.channel !== ""}
            helperText={
              filedError.channel !== "" ? `${filedError.channel}` : ""
            }
            required
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            label="Message"
            name="message"
            placeholder="Enter your message"
            defaultValue={message}
            onChange={handleChange("message")}
            margin="normal"
            error={filedError.message !== ""}
            helperText={
              filedError.message !== "" ? `${filedError.message}` : ""
            }
            // required
            multiline
            rows={4}
            // variant="filled"
          />
        </Grid>
      </Grid>
      <div
        style={{ display: "flex", marginTop: 50, justifyContent: "flex-end" }}
      >
        <Button
          variant="contained"
          disabled={!isEmpty || isError}
          color="primary"
          onClick={handleNext}
        >
          Next
        </Button>
      </div>
    </Fragment>
  );
};

export default SecondStep;
