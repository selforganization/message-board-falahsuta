import React from "react";
import Form from "./form/Form";
import { Grid, Breadcrumbs, Link, Typography } from "@material-ui/core";
import { useHistory } from "react-router-dom";

export default () => {
  const history = useHistory();

  const spacing = (num) => {
    return <div style={{ marginTop: "3px", width: "30px", height: num }}></div>;
  };

  return (
    <>
      {spacing(10)}
      <Grid container direction="column" justify="center" alignItems="center">
        <Grid>
          <Breadcrumbs separator="›" aria-label="breadcrumb">
            <Link
              color="inherit"
              style={{ cursor: "pointer" }}
              onClick={() => history.push("/")}
            >
              Home
            </Link>

            <Typography color="textPrimary">Post</Typography>
          </Breadcrumbs>
        </Grid>
      </Grid>
      <Form />
    </>
  );
};
