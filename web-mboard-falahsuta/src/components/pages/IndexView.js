import React, { useEffect, useState } from "react";
import axios from "axios";

import IndexCard from "./shared/IndexCard";
import { Grid, Breadcrumbs, Link, Typography } from "@material-ui/core";
import { useHistory } from "react-router-dom";

const MainMenu = () => {
  const [channels, setChannels] = useState([]);
  const history = useHistory();

  useEffect(() => {
    async function fetchData() {
      const response = await axios.get("http://localhost:4002/api/index");
      setChannels(response.data.channels);
    }

    fetchData();
  }, []);

  return (
    <>
      <Grid container direction="column" justify="center" alignItems="center">
        <Grid>
          <Breadcrumbs separator="›" aria-label="breadcrumb">
            <Link
              color="inherit"
              style={{ cursor: "pointer" }}
              onClick={() => history.push("/")}
            >
              Home
            </Link>

            <Typography color="textPrimary">Index</Typography>
          </Breadcrumbs>
        </Grid>
        <Grid>
          <h4
            style={{
              color: "rgb(130,130,130,1)",
              fontWeight: 400,
              marginRight: "320px",
              userSelect: "none",
            }}
          >
            Index:
          </h4>
          {Object.keys(channels).map((el) => {
            return (
              <div style={{ marginBottom: "7px" }}>
                <IndexCard chname={el} data={channels[el]} key={el} />
              </div>
            );
          })}
        </Grid>
      </Grid>
    </>
  );
};

export default React.memo(MainMenu);
